import * as React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { MainPage } from "./pages/MainPage/MainPage";
import { TestLocation } from "./pages/TestLocation/TestLocation";
import { ROUTES } from "./routes";

export function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={ROUTES.Main} element={<MainPage />} />
        <Route path={ROUTES.TestLocation} element={<TestLocation />} />
      </Routes>
    </BrowserRouter>
  );
}
