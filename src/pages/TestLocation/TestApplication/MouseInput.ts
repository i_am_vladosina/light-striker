import { Vector3 } from "three";
import { Camera } from "./Camera";
import { MouseCommand } from "./Commands";

// export interface MouseInputCommand {
//   isActive: boolean;
//   command: MouseCommand;
// }

export class RotateCamera {
  private camera: Camera;

  constructor(camera: Camera) {
    this.camera = camera;

    document.body.addEventListener("mousemove", (e) => {
      if (document.pointerLockElement === document.body) {
        this.camera.rotation.y -= e.movementX / 500;
        this.camera.rotation.x -= e.movementY / 500;
      }
    });

    document.body.addEventListener("mousedown", () => {
      document.body.requestPointerLock();
    });
  }
}
