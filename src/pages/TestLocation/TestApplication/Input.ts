import { Command } from "./Commands";

export interface Button {
  keyCode: string;
  isPressed: boolean;
  isEnabled: boolean;
  command: Command;
}

export class InputHandler {
  private buttons: Record<string, Button>;

  constructor(buttons: Button[]) {
    this.buttons = buttons.reduce((result, item) => {
      result[item.keyCode] = item;
      return result;
    }, {});

    window.addEventListener("keydown", this.handleKeyDown);
    window.addEventListener("keyup", this.handleKeyUp);
  }

  public disableButton(key: string) {
    this.buttons[key].isEnabled = false;
  }

  public enableButton(key: string) {
    this.buttons[key].isEnabled = true;
  }

  public removeListeners() {
    window.removeEventListener("keydown", this.handleKeyDown);
    window.removeEventListener("keyup", this.handleKeyUp);
  }

  public isPressed(key: string): boolean {
    return this.buttons[key].isPressed;
  }

  public isEnabled(key: string): boolean {
    return this.buttons[key].isEnabled;
  }

  public addCommand(btn: Button): void {
    this.buttons[btn.keyCode] = btn;
  }

  public removeCommand(btn: Button): void {
    delete this.buttons[btn.keyCode];
  }

  public handleInput(): void {
    Object.keys(this.buttons).forEach((key: string) => {
      if (this.isPressed(key) && this.isEnabled(key)) {
        this.buttons[key].command.execute();
      }
    });
  }

  private handleKeyDown = (e: KeyboardEvent): void => {
    this.buttons[e.key].isPressed = true;
  };

  private handleKeyUp = (e: KeyboardEvent): void => {
    this.buttons[e.key].isPressed = false;
  };
}
