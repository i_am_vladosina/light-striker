import { Vector3 } from "three";

interface Entity {
  shiftBy(pos: Vector3): void;
  moveTo(pos: Vector3): void;
  rotate(r: Vector3): void;
}

export interface Command {
  unit: Entity;

  execute(): void;
}

export interface MouseCommand {
  unit: Entity;

  execute(delta: Vector3): void;
}

export class MoveUnitCommand implements Command {
  private calcNewPosition: () => Vector3;
  public unit: Entity;

  constructor(unit: Entity, calcNewPosition: () => Vector3) {
    this.unit = unit;
    this.calcNewPosition = calcNewPosition;
  }

  public execute() {
    this.unit.moveTo(this.calcNewPosition());
  }
}

export class MouseClick implements Command {
  private calcNewPosition: () => Vector3;
  public unit: Entity;

  constructor(unit: Entity, calcNewPosition: () => Vector3) {
    this.unit = unit;
    this.calcNewPosition = calcNewPosition;
  }

  public execute() {
    this.unit.moveTo(this.calcNewPosition());
  }
}

export class RotationByMouse implements MouseCommand {
  private speed: Vector3;
  public unit: Entity;

  constructor(unit: Entity, speed: Vector3) {
    this.unit = unit;
    this.speed = speed;
  }

  public execute(delta: Vector3) {
    if (document.pointerLockElement === document.body) {
      // this.rotation.y -= event.movementX / 500;
      // this.rotation.x -= event.movementY / 500;
      const d = new Vector3(
        -delta.x * this.speed.x,
        -delta.y * this.speed.y,
        0
      );
      console.log(d);
      this.unit.rotate(d);
    }
  }
}

export class MouseStart implements MouseCommand {
  public unit: Entity;

  constructor(unit: Entity) {
    this.unit = unit;
  }

  public execute(delta: Vector3) {
    if (document.pointerLockElement === document.body) {
      document.body.requestPointerLock();
    }
  }
}
