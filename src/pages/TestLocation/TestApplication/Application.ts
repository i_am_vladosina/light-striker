import {
  PerspectiveCamera,
  Scene,
  Vector2,
  Vector3,
  WebGLRenderer,
} from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { MouseStart, MoveUnitCommand, RotationByMouse } from "./Commands";
import { InputHandler } from "./Input";
import { Platform } from "./Platform";
import { Camera } from "./Camera";
import { RotateCamera } from "./MouseInput";

export class Application {
  private containerNode: HTMLDivElement;
  private camera: Camera;
  private renderer: WebGLRenderer;
  private sceneSize: Vector2;
  private platform: Platform;
  private scene: Scene;
  private input: InputHandler;
  private mouseInput: RotateCamera;
  private playerDirection: Vector3;

  constructor(containerNode: HTMLDivElement) {
    this.containerNode = containerNode;

    this.camera = new Camera(75, this.calcSceneAspect(), 1, 100);
    this.camera.position.z = 30;

    //const controls = new OrbitControls(this.camera, containerNode);

    this.renderer = new WebGLRenderer();
    this.renderer.setClearColor(0xd3e0ec);
    const sceneSize = this.calcSceneSize();
    this.renderer.setSize(sceneSize.x, sceneSize.y);
    this.containerNode.appendChild(this.renderer.domElement);

    this.scene = new Scene();

    this.platform = new Platform();

    this.platform.addToScene(this.scene);

    this.observeResize();

    this.playerDirection = new Vector3();
    const speedDelta = 1;
    this.input = new InputHandler([
      {
        keyCode: "w",
        isEnabled: true,
        isPressed: false,
        command: new MoveUnitCommand(this.camera, () => {
          return this.camera.position.add(
            this.getForwardVector().multiplyScalar(speedDelta)
          );
        }),
      },
      {
        keyCode: "s",
        isEnabled: true,
        isPressed: false,
        command: new MoveUnitCommand(this.camera, () => {
          return this.camera.position.add(
            this.getForwardVector().multiplyScalar(-speedDelta)
          );
        }),
      },
      {
        keyCode: "d",
        isEnabled: true,
        isPressed: false,
        command: new MoveUnitCommand(this.camera, () => {
          return this.camera.position.add(
            this.getSideVector().multiplyScalar(speedDelta)
          );
        }),
      },
      {
        keyCode: "a",
        isEnabled: true,
        isPressed: false,
        command: new MoveUnitCommand(this.camera, () => {
          return this.camera.position.add(
            this.getSideVector().multiplyScalar(-speedDelta)
          );
        }),
      },
    ]);

    this.mouseInput = new RotateCamera(this.camera);

    this.render();
  }

  public destroy() {
    this.unobserveResize();
    this.input.removeListeners();
  }

  private render = () => {
    this.input.handleInput();
    this.renderer.render(this.scene, this.camera);
    window.requestAnimationFrame(this.render);
  };

  private getForwardVector() {
    this.camera.getWorldDirection(this.playerDirection);
    this.playerDirection.y = 0;
    this.playerDirection.normalize();

    return this.playerDirection;
  }

  private getSideVector() {
    this.camera.getWorldDirection(this.playerDirection);
    this.playerDirection.y = 0;
    this.playerDirection.normalize();
    this.playerDirection.cross(this.camera.up);

    return this.playerDirection;
  }

  private calcSceneAspect(): number {
    const sceneSize = this.calcSceneSize();

    return sceneSize.x / sceneSize.y;
  }

  private calcSceneSize(): Vector2 {
    const { width, height } = this.containerNode.getBoundingClientRect();

    return new Vector2(width, height);
  }

  private observeResize(): void {
    window.addEventListener("resize", this.handleResize);
  }

  private unobserveResize(): void {
    window.removeEventListener("resize", this.handleResize);
  }

  private handleResize = () => {
    this.setCanvasSize();
    this.camera.aspect = this.calcSceneAspect();
    this.camera.updateProjectionMatrix();
  };

  private setCanvasSize() {
    this.sceneSize = this.calcSceneSize();
    this.renderer.setSize(this.sceneSize.x, this.sceneSize.y);
  }
}
