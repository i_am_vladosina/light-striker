import { BoxGeometry, Mesh, MeshBasicMaterial, Scene, Vector3 } from "three";

export class Platform {
  private material: MeshBasicMaterial;
  private geometry: BoxGeometry;
  private mesh: Mesh;

  constructor() {
    this.material = new MeshBasicMaterial({ color: 0x477096 });
    this.geometry = new BoxGeometry(20, 1, 20);

    this.mesh = new Mesh(this.geometry, this.material);
  }

  public addToScene(scene: Scene) {
    scene.add(this.mesh);
  }

  public shiftBy(pos: Vector3) {
    this.mesh.position.add(pos);
  }

  public moveTo(pos: Vector3) {
    this.mesh.position.copy(pos);
  }

  public rotate(r: Vector3) {
    this.mesh.rotation.x = r.x;
    this.mesh.rotation.y = r.y;
    this.mesh.rotation.z = r.z;
  }
}
