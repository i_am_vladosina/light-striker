import { PerspectiveCamera, Scene, Vector3 } from "three";

export class Camera extends PerspectiveCamera {
  constructor(
    fov: number,
    aspect: number,
    near: number = 1,
    far: number = 1000
  ) {
    super(fov, aspect, near, far);

    // document.body.addEventListener("mousedown", () => {
    //   document.body.requestPointerLock();
    // });

    // document.body.addEventListener("mousemove", (event) => {
    //   if (document.pointerLockElement === document.body) {
    //     this.rotation.y -= event.movementX / 500;
    //     this.rotation.x -= event.movementY / 500;
    //   }
    // });
  }

  public shiftBy(pos: Vector3) {
    this.position.add(pos);
  }

  public moveTo(pos: Vector3) {
    this.position.copy(pos);
  }

  public rotate(r: Vector3) {
    this.rotation.x = r.x;
    this.rotation.y = r.y;
    this.rotation.z = r.z;
  }
}
