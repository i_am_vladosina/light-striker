import React, { useRef, useEffect } from "react";
import { Application } from "./TestApplication/Application";

import "./TestLocation.scss";

export function TestLocation() {
  const container = useRef<HTMLDivElement>();
  const application = useRef<Application>();

  useEffect(() => {
    if (container.current) {
      application.current = new Application(container.current);
      return () => {
        application.current.destroy();
      };
    }
  });

  return <div ref={container} className="root"></div>;
}
